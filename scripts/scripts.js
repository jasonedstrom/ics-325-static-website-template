/**
 * Created by jasonedstrom on 2/3/14.
 */


jQuery.noConflict ();
(function($) {
    $(document).ready(function() {
        $('#product1').on('click', function() {
            new Messi('<img class="productimg" src="http://s.w.org/about/images/logos/wordpress-logo-notext-rgb.png"><p style="font-weight: bold; float:right;">Price: $150</p><p style="text-align: center;">Bring the beauty of the internet to you in all its glory.</p>', {title: 'Wordpress Theme', modal: true, buttons: [{id: 0, label: 'Close', val: 'X'}]});
        });

        $('#product2').on('click', function() {
            new Messi('<img class="productimg" src="http://prolificphoto.com/wp-content/uploads/2013/01/Adobe-Photoshop-Logo.png"><p style="font-weight: bold; float:right;">Price: $150</p><p style="text-align: center;">Bring the beauty of the internet to you in all its glory.</p>', {title: 'Photoshop Guru', modal: true, buttons: [{id: 0, label: 'Close', val: 'X'}]});
        });
    });
})(jQuery);